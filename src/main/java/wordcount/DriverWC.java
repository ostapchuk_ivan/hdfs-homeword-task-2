package wordcount;

import mainHW.PreviousResultsDelete;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 *  Word count here only to see if hadoop even works on my local machine.
 *  todo delete this when task is done
 */

public class DriverWC extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new DriverWC(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        Logger logger = Logger.getLogger(DriverWC.class);
        BasicConfigurator.configure();

        Configuration conf = getConf();

        if(args.length != 2) {
            logger.fatal("usage DriverWC inputFileOrDirectory outputDirectory");
            return -1;
        }

        String input = args[0];
        String output = args[1];

        PreviousResultsDelete.previousResultsCheck(output);

        Job job = Job.getInstance(conf);

        job.setJarByClass(getClass());
        job.setJobName(getClass().getName());

        FileInputFormat.addInputPath(job, new Path(input));
        FileOutputFormat.setOutputPath(job, new Path(output));

        job.setMapperClass(MapperWC.class);
        job.setReducerClass(ReducerWC.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
