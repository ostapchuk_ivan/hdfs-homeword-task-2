package mainHW;

import java.io.File;

public class PreviousResultsDelete {
    public static void previousResultsCheck(String path) {
        File previousResults = new File(path);
        if (previousResults.exists()) {
            deleteDirectory(previousResults);
        }
    }

    private static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }
}
