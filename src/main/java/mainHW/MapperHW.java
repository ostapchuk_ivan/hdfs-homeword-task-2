package mainHW;

import org.apache.avro.mapred.AvroKey;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;


public class MapperHW extends Mapper<AvroKey<topLevelRecord>, NullWritable, Text , IntWritable> {
    private final IntWritable one = new IntWritable(1);
    private Text word = new Text();

    @Override
    protected void map(AvroKey<topLevelRecord> key,  NullWritable value, Context context) throws IOException, InterruptedException {
        String string = (key.datum().getChannel().toString());
        word.set(string);
        context.write(word, one);
    }
}

