package mainHW;

import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyInputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Task2
 *  todo •	Write MR secondary sort job which will sort all the lines in the dataset
 *              first by hotel_id and then by srch_ci and booking id (to avoid random sort in case of equal srch_ci).
 *
 *  todo •	Find marketing channel ("channel" column)
 *              for each hotel_id with the latest companies visit (2+ adult visitors).
 *
 *  todo •	Use Avro (Expedia) dataset from the first task (1.7.x version of avro tools is compatible with 3.x HDP).
 *
 *  todo •	Add MR Unit tests for your Mapper/Reducer.
 *
 *  todo •	Saved project and screenshot of execution at GitLab
 */

public class MainHW extends Configured implements Tool {
    public static void main(String[] args) throws Exception{
        int res = ToolRunner.run(new MainHW(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();

        if(args.length != 0 && args.length != 2) {
            System.out.println("Usage hdfsInputFileOrDirectory hdfsOutPutDirectory");
            return 100;
        }

        String hdfsInput = args[0];
        String hdfsOutput = args[1];

        PreviousResultsDelete.previousResultsCheck(hdfsOutput);

        Job job = Job.getInstance(conf);
        job.setJobName(getClass().getName());
        job.setJarByClass(getClass());

        FileOutputFormat.setOutputPath(job, new Path(hdfsOutput));
        AvroKeyInputFormat.addInputPath(job, new Path(hdfsInput));

        job.setInputFormatClass(AvroKeyInputFormat.class);
        job.setMapperClass(MapperHW.class);
        AvroJob.setInputKeySchema(job, topLevelRecord.getClassSchema());

        job.setReducerClass(ReducerHW.class);


        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
